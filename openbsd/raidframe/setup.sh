#!/bin/sh

set -x

if [ $# -ne 2 ]; then
	echo "usage: $0 [option file] [layout file]" >&2
	exit 1
fi

for _i in $@; do
	if [ -n "${_i%%/*}" ]; then
		echo "$0: '$_i' is not an absolute path" >&2
		exit 1
	fi
done

_option_file="$1"
_layout_file="$2"
shift 2

. "$_option_file"

_bc()
{
	echo "$@" | bc -l | xargs printf "%1.0f"
}

_sectors()
{
	case "$1" in
	*[bB])	_bc "${1%[bB]*}";;
	*[kK])	_bc "${1%[kK]*} * 2";;		# 2^10/512
	*[mM])	_bc "${1%[mM]*} * 2048";;	# 2^20/512
	*[gG])	_bc "${1%[gG]*} * 2097152";;	# 2^30/512
	*[tT])	_bc "${1%[tT]*} * 2147483648";;	# 2^40/512
	*)	echo $1;;
	esac
}

_disk_sectors()
{
	disklabel $1 2>/dev/null |
	awk '{
		if (tolower($0) ~ /total sectors/) {
			sub(/.*:/, "")
			print
			exit
		}
	}'

#	fdisk $1 2>/dev/null |
#	grep "Sectors" |
#	sed 's/.*\[\(.*\) Sectors\].*/\1/'
}

case $_detect_disknames_method in
mib)	_disknames=`sysctl -n hw.disknames | tr ',' ' '`;;
manual)	;;
esac
[ -z "$_disknames" ] && exit 1

#
#  Find two disks with the same size (# of sectors)
#
case $_detect_disks_method in
guess)
	set -- $_disknames
	while [ $# -gt 0 ]; do
		case $1 in
		[ws]d*)
			_disk_0=$1; shift
			_sectors_0=`_disk_sectors $_disk_0`
			[ -z "$_sectors_0" ] && continue
			for _i in $@; do
				_disk_1=$_i
				_sectors_1=`_disk_sectors $_i`
				[ -z "$_sectors_1" ] && continue
				if [ $_sectors_0 -eq $_sectors_1 ]; then
					shift $#
					break
				fi
			done
			;;
		*)	shift
			;;
		esac
	done
	;;
manual)
	_sectors_0=`_disk_sectors $_disk_0`
	_sectors_1=`_disk_sectors $_disk_1`
	[ $_sectors_0 -eq 0 -o $_sectors_0 -eq $_sectors_1 ] || exit 1
	;;
esac
[ -z "$_disk_0" -o -z "$_disk_1" ] && exit 1

if [ ! -f /bsd.old ]; then
	#
	#  Create working space, download, and compile kernel for RAID
	#
	[ $_wipe -eq 1 ] && dd if=/dev/zero of=/dev/r${_disk_1}c bs=32k
	fdisk -i -y $_disk_1
	disklabel $_disk_0 | disklabel -R $_disk_1 /dev/stdin || exit 1
	newfs /dev/r${_disk_1}a
	mount /dev/${_disk_1}a /mnt
	cd /mnt
	_i=0
	[ -f /tmp/$_src_file ] && mv /tmp/$_src_file .
	while [ ! -f $_src_file ]; do
		ftp -a -C "$_src_mirror/$_src_path/$_src_release/$_src_file"
		_i=`expr $_i + 1`
		[ $_i -gt $_ftp_trys ] && exit 1
	done
	gzip -dc $_src_file | tar -xf -
	cd sys/arch/`machine`/conf
	ed -s GENERIC <<-EOF >$_kern
	/raid/s/^#//
	a
	option RAID_AUTOCONFIG
	.
	,p
	EOF
	config $_kern
	cd ../compile/$_kern
	make depend
	make
	mv /bsd /bsd.old
	install -o root -g wheel -m 644 bsd /
	cd
	umount /mnt
	[ "$_restart" = "YES" ] && (shutdown -r +3)
fi

[ "`uname -v`" = "$_kern#0" ] || exit 1

# Label the second disk again in case the first stage was skipped
disklabel $_disk_0 | disklabel -R $_disk_1 /dev/stdin || exit 1

#
#  Determine the maximum number of partitions
#
case $_detect_max_partitions_method in
mib)	_max_partitions=`sysctl -n kern.maxpartitions`
	;;
header) [ -z "$_disklabel_header" ] && exit 1
	if [ -f $_disklabel_header ]; then
		_max_partitions=`awk '{
			if (($1 == "#define") && ($2 == "MAXPARTITIONS")) {
				print $3
			}
		}' $_disklabel_header`
	fi
	;;
guess)	_max_partitions=16
	;;
manual)
	;;
esac
[ -z "$_max_partitions" ] && exit 1

_max_partition=`jot -c $_max_partitions a | tail -n 1`
[ -z "$_max_partition" ] && exit 1

#
#  Determine the root device and partition
#
case $_detect_root_method in
guess)	_root=`mount | awk '{
		if (($1 ~ /\/dev\//) && ($3 == "/")) {
			sub(/\/dev\//, "")
			print $1
		}
	}'`
	_root_device=`echo $_root | sed 's/\([0-9]\)[a-z]/\1/'`
	_root_partition=`echo $_root | sed "s/$_root_device\([a-z]\)/\1/"`
	;;
manual)
	;;
esac
[ -z "$_root_device" -o -z "$_root_partition" ] && exit 1

#
#  Determine the RAID device
#
case $_detect_raid_device_method in
guess)	for _i in $_disknames; do
		case $_i in
		[ws]d*)
			if disklabel $_i 2>/dev/null |
			grep -qs "RAID"; then
				_raid_device=$_i
				break
			fi
			;;
		esac
	done
	;;
manual)
	;;
esac
[ -z "$_raid_device" ] && exit 1

#
# Determine the RAID partition
#
case $_detect_raid_partition_method in
guess)	_raid_partition=`disklabel $_raid_device | awk '{
		if (($1 ~ /[a-bd-'$_max_partition']:/) &&
		($4 == "RAID")) {
			printf "%c", $1
		}
	}'`
	;;
manual)
	;;
esac
[ -z "$_raid_partition" ] && exit 1

#
# Determine the RAID set number
#
case $_detect_raid_num_method in
mib)	_n=0
	for _i in $_disknames; do
		case $_i in
		raid*) _n=`expr $_n + 1`;;
		esac
	done
	_raid_num=$_n
	;;
conf)	_n=0
	for i in /etc/raid[0-9].conf; do
	    if [ -f /etc/raid$_n.conf ]; then
		_n=`expr $_n + 1`
	    fi
	done
	_raid_num=$_n
	;;
manual)
	;;
esac
[ -z "$_raid_num" ] && exit 1
_raid=raid$_raid_num

#
# Unset variables which will be used for partitions
#
unset `jot -w "_size_" -c 26 a`

#
# Determine the size of the raid partition
#
# XXX
_size_c=`disklabel $_raid_device |
    awk '{
	if ($1 == "'$_raid_partition':") {
	    print $2 - 127
	}
    }'`
[ -z "$_size_c" ] && exit 1
[ $_size_c -eq 0 ] && exit
_vfstype_c=unused
_offset_c=0

#
# Read in the layout
#
_num=1	# partition count; including `c'
_sum=0
_offset=0
_partitions=""
while read _line; do
	case "$_line" in
	""|'#'*)	continue;;
	esac
	set -- $_line
	[ $# -ne 5 ] && exit 1
	expr $1 '>' $_max_partition && exit 1
	[ "$1" = "c" ] && exit 1
	_size=`_sectors $2`
	if [ "$_size" = "." ]; then
		[ $_offset -gt $_size_c ] && exit 1
		# XXX
		_size=`_bc "$_size_c - $_offset - 1"`
	fi
	eval _offset_$1=$_offset
	eval _size_$1=$_size
	eval _vfstype_$1=$3
	eval _file_$1=$4
	eval _mntops_$1=$5
	_num=`expr $_num + 1`
	_sum=`expr $_sum + $_size`
	_offset=`expr $_offset + $_size`
	_partitions="$_partitions $1"
done < "$_layout_file"
[ $_num -eq 1 ] && exit
[ $_num -gt $_max_partitions ] && exit 1
[ $_sum -eq 0 ] && exit
[ $_sum -gt $_size_c ] && exit 1

_overflow_partition=$(jot -c `expr $_max_partitions + 1` a | tail -n 1)
_unusable_partitions=`expr 26 - $_max_partitions`
for _i in `jot -c $_unusable_partitions $_overflow_partition`; do
	[ -n "`eval echo -n '$'_size_$_i`" ] && exit 1
done

#
# RAID device parameters
#
_cps=1024
_cyl=`_bc "$_size_c / $_cps"`

#
# Create the RAID configuration file
#
[ -z "$_serial" ] && exit 1

cat > /etc/$_raid.conf <<EOF
START array
1 2 0
START disks
/dev/$_disk_0$_raid_partition
/dev/$_disk_1$_raid_partition
START layout
128 1 1 1
START queue
fifo 100
EOF

#
# Initialize the RAID set
#
raidctl -C /etc/$_raid.conf $_raid
raidctl -I $_serial $_raid
raidctl -iv $_raid

# 
# Create the RAID device disklabel
#
cat > /tmp/disklabel.$_raid <<EOF
# /dev/r${_raid}c:
type: raid
disk: raid
label: fictitious
flags:
bytes/sector: 512
sectors/track: 128
tracks/cylinder: 8
sectors/cylinder: $_cps
cylinders: $_cyl
total sectors: $_size_c 
rpm: 3600
interleave: 1
boundstart: 0
boundend: $_size_c 
drivedata: 0 

16 partitions:
EOF

for _i in $_partitions c; do
	_size=`eval echo -n '$'_size_$_i`
	_offset=`eval echo -n '$'_offset_$_i`
	_fstype=`eval echo -n '$'_vfstype_$_i`
	# rest = fsize bsize cpg/sgs
	case $_fstype in
	swap|unused)	_rest=;;
	4.2BSD)		_rest="0 0 0";;
	esac
	echo "$_i: $_size $_offset $_fstype $_rest" >> /tmp/disklabel.$_raid
done

yes | disklabel -R $_raid /tmp/disklabel.$_raid || exit 1
rm /tmp/disklabel.$_raid

mv -f /etc/fstab /etc/fstab.old
:> /etc/fstab

#
# Create, mount, copy, and record the filesytem table entries for the partitions
#
for _i in $_partitions; do
	_fs_vfstype=`eval echo -n '$'_vfstype_$_i`
	_fs_file=`eval echo -n '$'_file_$_i`
	_fs_mntops=`eval echo -n '$'_mntops_$_i`

	if [ "$_fs_vfstype" = "4.2BSD" -a "$_fs_file" != "none" ]; then
		mkdir -p /$_fs_file
		newfs /dev/r$_raid$_i
		if [ "$_fs_file" != "/tmp" ]; then
			mkdir -p /tmp/$_fs_file
			mount /dev/$_raid$_i /tmp/$_fs_file
			dump -0 -f - $_fs_file | (cd /tmp; restore -r -f -)
			umount /tmp/$_fs_file
			rmdir /tmp/$_fs_file
		fi
	fi

	case $_fs_vfstype in
	none)	continue
		;;
	swap)	_fs_vfstype="swap"
		_fs_freq=0
		_fs_passno=0
		;;
	4.2BSD) _fs_vfstype="ffs"
		_fs_freq=1
		if [ "$_fs_file" = "/" ]; then
			_fs_passno=1
			_raid_root=$_raid$_i
		else
			_fs_passno=2
		fi
		;;
	esac

	echo "/dev/$_raid$_i $_fs_file $_fs_vfstype $_fs_mntops"\
		"$_fs_freq $_fs_passno" >> /etc/fstab
done

#
# Copy fstab to RAID root partition
#
if [ -n "$_raid_root" ]; then
	mount /dev/$_raid_root /mnt
	cp /etc/fstab /mnt/etc/fstab
	umount /mnt
	raidctl -A root $_raid
fi

#cat > /etc/boot.conf <<EOF
#boot hd1a:/bsd
#EOF

#
# Edit /etc/rc to background parity check (potentially unsafe)
#
mv /etc/rc /etc/rc.old
sed 's/^raidctl -P all/(&)\&/' /etc/rc.old > /etc/rc

#
# Copy real root (boot device) to second disk
#
newfs /dev/r$_disk_1$_root_partition
mount /dev/$_disk_1$_root_partition /mnt
(cd /mnt; dump -0f - / | restore -rf -)
/usr/mdec/installboot -v /mnt/boot /usr/mdec/biosboot $_disk_1
umount /mnt

[ "$_restart" = "YES" ] && (shutdown -r +3)
