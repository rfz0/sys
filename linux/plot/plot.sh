#!/bin/bash

plot()
{
	(
		cat plot.gpi
		if [ "$2" ]; then
			awk -v n=$2 'END{printf "set xrange [\"%d\":\"%d\"]\n",$1-n,$1}' $1.dat
		fi
		cat $1.gpi
	) | gnuplot | tee $1.png | display
}

plot_cpus()
{
	(
		cat plot.gpi
		if [ "$3" ]; then
			awk -v n=$3 'END{printf "set xrange [\"%d\":\"%d\"]\n",$1-n,$1}' cpu0.dat
		fi
		echo 'set title "cpu % used"'
		echo 'set ylabel "%"'
		echo -n "plot "
		local i=$1
		local n=$2
		while ((i < n)); do
			echo -n "'cpu$i.dat' using 1:7 title '$i' with lines"
			if ((i < (n - 1))); then
				echo ", \\"
			else
				echo
			fi
			((i++))
		done
	) | gnuplot | tee cpus.png | display
}

plot_ifaces()
{
	(
		cat plot.gpi
		if [ "$1" ]; then
			awk -v n=$1 'END{printf "set xrange [\"%d\":\"%d\"]\n",$1-n,$1}' net-lo.dat
		fi
		echo 'set title "net"'
		echo 'set ylabel "bps"'
		echo -n "plot "
		local d
		local f
		local i
		local n
		i=0
		n=`ls net-*.dat | wc -l`
		for f in `ls net-*.dat`; do
			d=${f%.*}
			d=${d#*-}
			echo "'$f' using 1:3 title '$d rx' with lines, \\"
			echo -n "'$f' using 1:4 title '$d tx' with lines"
			if ((i < (n - 1))); then
				echo ", \\"
			else
				echo
			fi
			((i++))
		done
	) | gnuplot | tee ifaces.png | display
}

plot_disks()
{
	(
		cat plot.gpi
		if [ "$1" ]; then
			awk -v n=$1 'END{printf "set xrange [\"%d\":\"%d\"]\n",$1-n,$1}' disk-sda.dat
		fi
		echo 'set title "disk"'
		echo 'set ylabel "bps"'
		echo -n "plot "
		local d
		local f
		local i
		local n
		i=0
		n=`ls disk-*.dat | wc -l`
		for f in `ls disk-*.dat`; do
			d=${f%.*}
			d=${d#*-}
			echo "'$f' using 1:3 title '$d rd' with lines, \\"
			echo -n "'$f' using 1:4 title '$d wr' with lines"
			if ((i < (n - 1))); then
				echo ", \\"
			else
				echo
			fi
			((i++))
		done
	) | gnuplot | tee disks.png | display
}

case "$1" in
cpu|load|mem|swap)
	plot "$1" $2
	;;
cpus)
	plot_cpus 0 `ls cpu[0-9]*.dat | wc -l` $2
	;;
disks)
	plot_disks $2
	;;
ifaces)
	plot_ifaces $2
	;;
*)
	echo "usage: $0 [cpu|cpus|disks|ifaces|load|mem|swap] [delta (seconds)]" >&2
	exit 1
	;;
esac

