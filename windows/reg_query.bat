@echo off

SET key=%~1
SET val=%~2

CALL :reg_read "%key%" "%val%" || ( EXIT /B 1 )

IF {%RETURN}=={} (
	echo Value not found >&2
) ELSE (
	ECHO %RETURN%
)

EXIT /B 0

:reg_read <key> <value>
SETLOCAL

SET key=%~1
SET val=%~2

REG QUERY "%key%" /v "%val%" 1>nul 2>&1 || ( EXIT /B 1 )

FOR /f "tokens=2,*" %%a IN ('REG QUERY "%key%" /V "%val%" ^| FINDSTR /c:"%val%"') do (
	SET data=%%b
)

REM // IF {%data%}=={} ( ENDLOCAL & EXIT /B 1 )

ENDLOCAL & SET RETURN=%data%
EXIT /B 0
