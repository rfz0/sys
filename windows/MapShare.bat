@echo off

if not [%1] == [] (
	echo usage: %0% >&2
) else (
	call :main
)
pause & goto :eof

:main
setlocal
set "drive="
set host=HOSTNAME
set share=SHARENAME
set user=USERNAME
set pass=PASSWORD

call :findfree drive
if [%drive%] == [] (
	echo %0%: no free drives found >&2
) else (
	call :mapdrive %drive% %user% %host% %share% %pass%
)
endlocal
goto :eof

:mapdrive <drive> <user> <host> <share> <pass>
setlocal
:: XXX doesn't work on some devices (e.g., cdrom drive)
if not exist %~1 (
	net use %~1 /user:%~2 \\%~3\%~4 %~5
) else (
	echo %0%: drive '%~1' is already in use >&2
)
endlocal
goto :eof

:findfree <arg>
setlocal
set a=z y x w v u t s r q p o m l k j i h g f e d c b a
for %%i in (%a%) do (
	if not exist %%i: >nul (
		set x=%%i:
		goto end
	)
)
:end
endlocal & set "%~1=%x%"
goto :eof
