#!/bin/sh

# Move single-partition system to multi-partition CGD.
# Must have enough free space after install partition.

set -x

_cgd_alg=aes-cbc
_cgd=cgd0
_cgd_keysize=256
_disk=`sysctl -n kern.root_device`
_max_partitions=`sysctl -n kern.maxpartitions`
_mfs_tmp_size=128m
_mfs_tmp_opts="rw,async,noatime,nosuid,noexec,-s=$_mfs_tmp_size"
_mfs_tmp=YES
_restart=NO
_use_free=YES
_layout="
# PART		SIZE		TYPE		DIR		OPTS
b	   	2g		swap		none		sw
#c
#d
e	   	4.5g		4.2BSD  	/usr		rw
f		100m		4.2BSD		/usr/local	rw
f	   	128m		4.2BSD  	/var		rw
g		.		4.2BSD		/home		rw
"
_fstab_extra="
kernfs /kern kernfs rw
ptyfs /dev/pts ptyfs rw
procfs /proc procfs rw
/dev/cd0a /cdrom cd9660 ro,noauto
/dev/sd0i /usb msdos rw,noauto
"

_bc()
{
        echo "$@" | bc -l | xargs printf "%1.0f"
}

_sectors()
{
	case "$1" in
	*[bB])	_bc ${1%[bB]*};;			# blocks, NOT bytes
	*[kK])	_bc ${1%[kK]*} '*' 2;;
	*[mM])	_bc ${1%[mM]*} '*' 2048;;
	*[gG])	_bc ${1%[gG]*} '*' 2097152;;
	*[tT])	_bc ${1%[tT]*} '*' 2147483648;;
	*)	echo $1;;
	esac
}

_max_partition=`jot -c $_max_partitions a | tail -n 1`

_disk_size=`disklabel $_disk |
awk '{
	if (tolower($0) ~ /total sectors/) {
		sub(/.*:/, "")
		print
		exit
	}
}'`

_last=`disklabel $_disk 2>/dev/null |
awk -v use_free=$_use_free 'BEGIN { last="" }
{
	if ($1 ~ /[a-be-'$_max_partition']:/) {
		if ((tolower(use_free) != "yes") && (tolower($4) == "ccd")) {
			exit
		}
		if (tolower($4) == "4.2bsd") {
			sub(/:/,"")
			last=$1
		}
	}
}
END { print last }'`
[ -z "$_last" ] && exit 1

if [ "$_last" = "b" ]; then
	_ccd_partition="e"
else
	_ccd_partition=`jot -c 2 $_last | tail -n 1`
fi
expr $_ccd_partition '>' $_max_partition && exit 1
_ccd=$_disk$_ccd_partition
_ccd_offset=`disklabel $_disk |
	awk '{ if ($1 ~ /^'$_last':/) { print $2 + $3 } }'`
_ccd_size=`expr $_disk_size - $_ccd_offset`
[ $_ccd_size -eq 0 ] && exit 1
(
	disklabel $_disk
	echo "$_ccd_partition: $_ccd_size $_ccd_offset ccd 0 0 0"
) | disklabel -R -r $_disk /dev/stdin

unset `jot -w "_size_" -c 26 a`

_num=2	  # partitions; c + d
_sum=0
_offset=0
_partitions=""
while read _line; do
	case "$_line" in
	""|'#'*)	continue;;
	esac
	set -- $_line
	[ $# -ne 5 ] && exit 1
	expr $1 '>' $_max_partition && exit 1
	[ "$1" = "c" -o "$1" = "d" ] && exit 1
	_size=`_sectors $2`
	if [ "$_size" = "." ]; then
	    _size=`eval "echo \"$_ccd_size - ($_offset)\" | bc "`
	fi
	eval _size_$1=$_size
	eval _offset_$1=$_offset
	eval _fstype_$1=$3
	eval _file_$1=$4
	eval _opts_$1=$5
	_num=`expr $_num + 1`
	_sum=`expr $_sum + $_size`
	_offset=`expr $_offset + $_size`
	_partitions="$_partitions $1"
done <<EOF
$_layout
EOF

[ $_num -eq 2 -o $_num -gt $_max_partitions ] && exit 1
[ $_sum -eq 0 -o $_sum -gt $_ccd_size ] && exit 1

_fstype_d=unused

[ -n "$_size_c" ] && exit 1

_cyl=`_bc $_ccd_size / 2048`

cgdconfig -s $_cgd /dev/$_ccd $_cgd_alg $_cgd_keysize < /dev/urandom || exit 1
dd if=/dev/zero ibs=64k | progress -l ${_ccd_size}b \
	dd of=/dev/r${_cgd}d obs=64k
cgdconfig -u $_cgd || exit 1
mkdir -p /etc/cgd
cgdconfig -g -V disklabel -o /etc/cgd/$_ccd $_cgd_alg $_cgd_keysize || exit 1
until cgdconfig -V re-enter $_cgd /dev/$_ccd; do :; done
cat >> /etc/cgd/cgd.conf <<EOF
$_cgd /dev/$_ccd
EOF

cat > /tmp/disklabel.$_cgd <<EOF
type: cgd
disk: cgd
label: fictitious
flags:
bytes/sector: 512
sectors/track: 2048
tracks/cylinder: 1
sectors/cylinder: 2048
cylinders: $_cyl
total sectors: $_ccd_size
rpm: 3600
interleave: 1
trackskew: 0
cylinderskew: 0
headswitch: 0	   # microseconds
track-to-track seek: 0  # microseconds
drivedata: 0 

$_num partitions:
EOF

for _i in $_partitions d; do
	if [ "$_i" = "d" ]; then
		_size=$_ccd_size
		_offset=0
		_fstype=unused
	else
		_size=`eval echo -n '$'_size_$_i`
		_offset=`eval echo -n '$'_offset_$_i`
		_fstype=`eval echo -n '$'_fstype_$_i`
	fi
	# rest = fsize bsize cpg/sgs
	case $_fstype in
	swap)   _rest=;;
	unused) _rest="0 0";;
	4.2BSD) _rest="0 0 0";;
	esac
	echo "$_i: $_size $_offset $_fstype $_rest" >> /tmp/disklabel.$_cgd
done

disklabel -R -r $_cgd /tmp/disklabel.$_cgd || exit 1
rm /tmp/disklabel.$_cgd

for _i in $_partitions; do
	_fs_fstype=`eval echo -n '$'_fstype_$_i`
	_fs_file=`eval echo -n '$'_file_$_i`
	_fs_opts=`eval echo -n '$'_opts_$_i`

	if [ "$_fs_fstype" = "4.2BSD" -a "$_fs_file" != "none" ]; then
		mkdir -p /$_fs_file
		newfs /dev/r${_cgd}$_i
		if [ "$_fs_file" != "/tmp" ]; then
			mkdir -p /tmp/$_fs_file
			mount /dev/${_cgd}$_i /tmp/$_fs_file
			mkdir -p /$_fs_file
			dump -0 -f - $_fs_file | (cd /tmp; restore -r -f -)
			umount /tmp/$_fs_file
			rmdir /tmp/$_fs_file
		fi
	fi

	case "$_fs_fstype" in
	4.2BSD) _fs_vfstype="ffs"; _fs_freq=1; _fs_pass=2;;
	swap)   _fs_vfstype="swap"; _fs_freq=0; _fs_pass=0;;
	esac

	echo "/dev/$_cgd$_i $_fs_file $_fs_vfstype $_fs_opts $_fs_freq $_fs_pass" >> /etc/fstab

	case $_fs_file in
	/usr*|/var*)	   _critical_filesystems_local="$_critical_filesystems_local $_fs_file";;
	esac
done

while read _line; do
	case "$_line" in
	""|'#'*)	continue ;;
	esac
	set -- $_line
	mkdir -p "$2"
	if ! egrep -qs "^$1" /etc/fstab; then
	    echo "$_line" >> /etc/fstab
	fi
done <<EOF
$_fstab_extra
EOF

if [ "$_mfs_tmp" = "YES" ]; then
	for _i in $_partitions; do
		case `eval echo -n '$'_fstype_$_i` in
		swap)
		_size=`eval echo -n '$'_size_$_i`
		if [ `_sectors $_mfs_tmp_size` -le $_size ]; then
			echo "/dev/${_cgd}$_i /tmp mfs $_mfs_tmp_opts 0 0" \
				>> /etc/fstab
			break
		fi
		;;
		esac
	done
fi

for _i in cgd swapoff; do
	if ! egrep -qs "^$_i=YES" /etc/rc.conf; then
	    echo $_i=YES >> /etc/rc.conf
	fi
done

echo "critical_filesystems_local=\"\$critical_filesystems_local $_critical_filesystems_local\"" >> /etc/rc.conf

[ "$_restart" = "YES" ] && (shutdown -r +3)
