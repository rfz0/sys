#include <windows.h>
#include <stdio.h>

#include "template.h"

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
BOOL CALLBACK AboutDlgProc(HWND, UINT, WPARAM, LPARAM);

void ExitSystemError(LPCTSTR, LPCTSTR);

int AskConfirmation(HWND, LPCTSTR, LPCTSTR);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, INT iCmdShow)
{
	HWND hWnd;
	MSG msg;
	WNDCLASSEX wndClass;
	HANDLE hMutex;
	static TCHAR szAppName[] = TEXT("app");

	hMutex = CreateMutex(NULL, TRUE, szAppName);
	if (GetLastError() == ERROR_ALREADY_EXISTS)
	{
		if (AskConfirmation(NULL, TEXT("An instance of the program is already running.\n")
			TEXT("Really continue?"), szAppName) == IDNO)
			return EXIT_SUCCESS;
	}

	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.style = CS_HREDRAW | CS_VREDRAW;
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.hInstance = hInstance;
	wndClass.lpfnWndProc = WndProc;
	wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.lpszClassName = szAppName;
	wndClass.lpszMenuName = szAppName;
	wndClass.hbrBackground = (HBRUSH) GetStockObject(WHITE_BRUSH);
	wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	if (!RegisterClassEx(&wndClass))
		ExitSystemError(TEXT("RegisterClassEx"), szAppName);

	hWnd = CreateWindowEx(WS_EX_CLIENTEDGE, szAppName,
		szAppName, WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, CW_USEDEFAULT,
		CW_USEDEFAULT, CW_USEDEFAULT,
		NULL, NULL, hInstance, NULL);

	if (!hWnd)
		ExitSystemError(TEXT("CreateWindowEx"), szAppName);

	ShowWindow(hWnd, iCmdShow);
	UpdateWindow(hWnd);

	while (GetMessage(&msg, NULL, 0, 0) > 0)
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return msg.wParam;
}

void ExitSystemError(LPCTSTR lpcszFailure, LPCTSTR lpcszCaption)
{
	LPVOID lpMsgBuf, lpDispBuf;
	DWORD dwMsgId,
	      dwMsgFlags = FORMAT_MESSAGE_ALLOCATE_BUFFER|
		FORMAT_MESSAGE_FROM_SYSTEM|
		FORMAT_MESSAGE_IGNORE_INSERTS;

	lpMsgBuf = FormatMessage(dwMsgFlags, NULL, dwMsgId, 
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR) & lpMsgBuf, 0, NULL);

	lpDispBuf = (LPVOID) LocalAlloc(LMEM_ZEROINIT,
		(lstrlen((LPCTSTR) lpMsgBuf) + lstrlen(lpcszFailure)
		+ 40) * sizeof (TCHAR));

	MessageBox(NULL, (LPCTSTR) lpDispBuf, lpcszCaption, 
		MB_ICONERROR);
	
	fprintf(stderr, "%s: %s\n", (char*) lpcszCaption, (char*) lpDispBuf);

	LocalFree(lpMsgBuf);
	LocalFree(lpDispBuf);

	ExitProcess(dwMsgId);
}

int AskConfirmation(HWND hWnd, LPCTSTR lpcszQuery, LPCTSTR lpcszCaption)
{
	return MessageBox(hWnd, lpcszQuery, lpcszCaption,
		MB_YESNO | MB_ICONQUESTION);
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	static TCHAR lpszClassName[10];
	static HINSTANCE hInstance;
	static HWND hWndEdit;
	static HWND hWndStatic;

	switch (uMsg)
	{
		case WM_CREATE:
			GetClassName(hWnd, lpszClassName, 
				(int) sizeof lpszClassName / sizeof (TCHAR));
			hInstance = ((LPCREATESTRUCT)lParam)->hInstance;

			hWndStatic = CreateWindowEx(WS_EX_CLIENTEDGE,
				TEXT("static"), NULL, WS_CHILD | WS_VISIBLE,
				0, 0, 10, 0, hWnd, (HMENU) ID_STATIC, hInstance, NULL);
			hWndEdit = CreateWindowEx(WS_EX_CLIENTEDGE,
				TEXT("edit"), NULL, WS_CHILD | WS_VISIBLE | WS_HSCROLL | WS_VSCROLL
				| WS_BORDER | ES_LEFT | ES_MULTILINE | ES_AUTOHSCROLL | ES_AUTOVSCROLL,
				11, 0, 0, 0, hWnd, (HMENU) ID_EDIT, hInstance, NULL);
			return 0;

		case WM_SETFOCUS:
			SetFocus(hWndEdit);
			return 0;

		case WM_SIZE:
			MoveWindow(hWndStatic, 0, 0, LOWORD(lParam), HIWORD(lParam), TRUE); 
			MoveWindow(hWndEdit, 11, LOWORD(wParam)-10, LOWORD(lParam), HIWORD(lParam), TRUE);
			return 0;

		case WM_COMMAND:
			switch (LOWORD(wParam))
			{
				case IDM_APP_EXIT:
					SendMessage(hWnd, WM_CLOSE, 0, 0);
					return 0;

				case IDM_APP_ABOUT:
					
					DialogBoxParam(hInstance, TEXT("AboutBox"),
						hWnd, AboutDlgProc, (LPARAM) NULL);
					
					return 0;
			}
			break;

		case WM_CLOSE:
			if (AskConfirmation(hWnd, "Really Quit?", lpszClassName) 
				== IDYES)
				DestroyWindow(hWnd);
			return 0;

		case WM_QUERYENDSESSION:
			if (AskConfirmation(hWnd, "Really Quit?", lpszClassName)
				== IDYES)
			return 1;
		return 0;

		case WM_DESTROY:
			PostQuitMessage(0);
			break;
		
		default:
			return DefWindowProc(hWnd, uMsg, wParam, lParam);
	}
	
	return 0;
}

BOOL CALLBACK AboutDlgProc(HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch (uMsg)
	{
		case WM_INITDIALOG:
			return FALSE;

		case WM_COMMAND:
			switch (LOWORD(wParam))
			{
				case IDOK:
					EndDialog(hDlg, TRUE);
					return TRUE;
				case IDCANCEL:
					EndDialog(hDlg, FALSE);
					return TRUE;
			}
			break;
	}

	return FALSE;
}
