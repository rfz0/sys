#!/bin/sh

set -x

current()
{
	curl -s http://www.openbsd.org/index.html |
	grep "current release" |
	sed 's/.*>\([^<]*\)<.*/\1/' |
	cut -d " " -f 2
}

#: ${rel:=`echo $(uname -r) + .1 | bc -l`}
: ${rel:=`current`}
: ${arch:=`arch -s`}
: ${curl:="curl -C - -O"}
# XXX
: ${dev:=`dmesg | awk '/direct removable serial/ { print $1; exit}'`}
: ${mirror:=http://mirror.esc7.net}
: ${src:="ports.tar.gz src.tar.gz sys.tar.gz xenocara.tar.gz"}
: ${srel:=`echo $rel | tr -d .`}
#: ${url:=http://www.openbsd.org/}
: ${url:=http://129.128.5.191}
: ${vnd:=`vnconfig -l | grep "not in use" | cut -d: -f1 | head -1`}

[ `id -g` -eq 0 ] || exit 1

mkdir -p $rel
cd $rel
$curl "$url/faq/upgrade$srel.html"
$curl "$url/plus$srel.html"
$curl "$url/errata$srel.html"
$curl "$mirror/pub/OpenBSD/$rel/$arch/install$srel.iso"
for i in $src; do
	$curl "$mirror/pub/OpenBSD/$rel/$i"
done
dd if=/dev/zero of=/dev/r{$dev}c bs=1m count=8
fdisk -iy $dev
printf "a\n\n\n\n\nq\ny\n" | disklabel -E $dev
newfs /dev/r${dev}a
fsck -fp /dev/r${dev}a
mount /dev/${dev}a /mnt
cp /usr/mdec/boot /mnt
/usr/mdec/installboot -v /mnt/boot /usr/mdec/biosboot $dev
vnconfig $vnd "$rel/install$srel.iso" || exit 1
mkdir -p /cdrom
mount /dev/${vnd}a /cdrom
cp /cdrom/$rel/$arch/bsd.rd /mnt/bsd
mkdir -p /mnt/$rel/$arch
cp /cdrom/$rel/$arch/* /mnt/$rel/$arch
cp $rel/*.tar.gz /mnt/$rel
umount /cdrom
vnconfig -u $vnd
umount /mnt
