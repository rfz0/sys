#!/bin/sh

# Create NetBSD live usb file structure from installation ISO.
# Based on http://imil.net/nlk/build-HOWTO.txt.
# Requires a custom kernel to built with the options specified in the above document.

set -x

MACH=`machine`
NAME=netbsd
BOOTDISK=bootdisk
BOOTIMAGE=bootdisk.img
BS=512
ROOTDISKSIZE=262144
BOOTDISKSIZE=32768
DISKTAB="$NAME:\
_:ty=floppy:se#512:nt#1:rm#3600:ns#DISKSIZE:nc#1:\
_:pa#DISKSIZE:oa#0:ba#4096:fa#512:ta=4.2BSD:\
:pb#DISKSIZE:ob#0:\
:pc#DISKSIZE:oc#0:"
FAKEROOT=fakeroot
FAKEROOT_DIRS="bin dev etc home lib mnt sbin tmp usr usr/bin \
usr/lib usr/libexec usr/sbin var"
FAKEROOT_LIBFILES="libc.so libedit.so libtermcap.so libutil.so \
libkvm.so libcrypt.so libm.so libz.so"
#FAKEROOT_LIBEXECFILES="ld.elf_so"
FAKEROOT_USR_LIBFILES="libc.so libm.so libutil.so libz.so"
FAKEROOT_USR_LIBEXECFILES="ld.elf_so ld.so"
FAKEROOTFILES="bin/chmod bin/ln bin/mkdir bin/pax bin/rmdir bin/sh dev/MAKEDEV etc/rc \
sbin/disklabel sbin/dmesg sbin/init sbin/mknod sbin/mount sbin/mount_ffs \
sbin/mount_mfs sbin/mount_msdos sbin/reboot sbin/sysctl sbin/umount \
usr/bin/awk usr/bin/ldd usr/bin/tar usr/sbin/mtree usr/sbin/vnconfig"
INITFILES=initfiles
ISO="$1"
KERNEL=LIVEKEY
KERNEL_DIRECTORY=/usr/src/sys/arch/$MACH/compile/$KERNEL
MDNAME=mdisk.img
MFDISKSIZE=16m
MOUNTPOINT=/mnt
ROOTIMAGE=$NAME.img
ROOTZIMAGE=$NAME.zimg
SETS="base etc xbase xetc xserver"
RW_DIRS="dev etc var"
FILES="$BOOTDISK $BOOTIMAGE $FAKEROOT $INITFILES $MDNAME $NAME \
$ROOTIMAGE $ROOTZIMAGE menu.lst bsd"
VND0=vnd0
VND1=vnd1
SIZE_TMP=$ROOTDISKSIZE
SIZE_DEV=12000
SIZE_VAR=$ROOTDISKSIZE
SIZE_ETC=131072
SIZE_HOME=$ROOTDISKSIZE
SIZE_PKG=$ROOTDISKSIZE

if [ "$1" = "--clean" ]; then
	rm -rfv $FILES
	exit
fi

if [ -z $ISO ] || [ ! -f $ISO ]; then
	echo "usage: ${0##*/} [ISO (e.g., i386cd-5.1.iso)]" >&2
	exit 2
fi

if [ ! -f $KERNEL_DIRECTORY/netbsd ]; then
	echo "${0##*/}: $KERNEL_DIRECTORY/netbsd does not exist" >&2
	exit 1
fi

rm -rf $FILES
dd if=/dev/zero ibs=$BS | progress -b $BS -l ${ROOTDISKSIZE}k \
	dd of=$ROOTIMAGE count=$ROOTDISKSIZE obs=$BS
vnconfig $VND0 $ROOTIMAGE
echo "$DISKTAB" | sed "s/DISKSIZE/$ROOTDISKSIZE/g" |
	disklabel -w -f /dev/stdin /dev/r$VND0 $NAME
newfs -m 0 /dev/r${VND0}a
mkdir $NAME
mount /dev/${VND0}a $NAME
vnconfig -c $VND1 $ISO
mount -t cd9660 /dev/${VND1}a /cdrom
(
	cd $NAME
	for i in $SETS; do
		progress -zf /cdrom/$MACH/binary/sets/$i.tgz tar -xf-
	done
	mkdir usr/pkg
)
umount /cdrom
vnconfig -u $VND1
mkdir $FAKEROOT
(
	cd $FAKEROOT
	for i in $FAKEROOT_DIRS; do
		mkdir $i
	done
)
for i in $FAKEROOTFILES; do
	cp $NAME/$i $FAKEROOT/$i
	j=`readlink $NAME/$i`
	if [ -n "$j" ] && [ x${j##*/} != x${i##*/} ]; then
		cp $NAME/$j $FAKEROOT/$j
	fi
done
for i in $FAKEROOT_LIBFILES; do
	cp $NAME/lib/$i* $FAKEROOT/lib/
done
cp -r $NAME/libexec $FAKEROOT/
for i in $FAKEROOT_USR_LIBFILES; do
	cp $NAME/usr/lib/$i* $FAKEROOT/usr/lib/
done
for i in $FAKEROOT_USR_LIBEXECFILES; do
	cp $NAME/usr/libexec/$i* $FAKEROOT/usr/libexec/
done
(cd $FAKEROOT/dev; sh MAKEDEV all std)
echo '/dev/${VND0}a / ffs rw,noatime,nodevmtime 1 1' > $FAKEROOT/etc/fstab
cat > fakeroot/etc/rc <<EOF
trap "sh" INT
export PATH=/sbin:/bin:/usr/sbin:/usr/bin
INITFILES=$INITFILES
MOUNTPOINT=$MOUNTPOINT
NAME=$NAME
EOF
cat >> fakeroot/etc/rc <<"EOF"
FILESPATH=$MOUNTPOINT/$NAME
DISKS="`sysctl -n hw.disknames`"
[ -z "$DISKS" ] && DISKS="NONE"
while [ -z "$DEV" ]; do
	echo -n "Enter device ($DISKS): "
	read DEV
done
disklabel $DEV | awk '/size.*offset.*fstype.*fsize.*bsize.*cpg\/sgs/,0'
PARTS="`disklabel $DEV 2>/dev/null |
	awk 'BEGIN { p = "" } {
		if ($0 ~ /^ [a-z]:/) {
			sub(/:/, "")
			p = p " " $1
		}
	} END { print p }'`"
[ -z "$PARTS" ] && PARTS="NONE"
while [ -z "$PART" ]; do
	echo -n "Enter partition ($PARTS): "
	read PART
done
mount_msdos /dev/$DEV$PART $MOUNTPOINT
vnconfig -z vnd0 $FILESPATH/$NAME.zimg
mount_ffs -o ro,union /dev/vnd0a / >/dev/null 2>&1
EOF
cat >> fakeroot/etc/rc <<EOF
mount_mfs -s $SIZE_TMP swap /tmp
mount_mfs -s $SIZE_DEV swap /dev
mount_mfs -s $SIZE_VAR swap /var
mount_mfs -s $SIZE_ETC swap /etc
mount_mfs -s $SIZE_HOME swap /home
mount_mfs -s $SIZE_PKG swap /usr/pkg
EOF
cat >> fakeroot/etc/rc <<"EOF"
(cd / && tar -zxvf $FILESPATH/$INITFILES.tgz)
(cd /dev && sh MAKEDEV all std)
cd /
#for i in $FILESPATH/pkg/*.tgz; do
#	pkg_add -f $i >/dev/null 2>&1
#done
sh /etc/rc
EOF
mkdir $INITFILES
(
	cd $NAME
	mv $RW_DIRS ../$INITFILES
	mkdir $RW_DIRS
)
ed -s $INITFILES/etc/rc.d/root <<-EOM
	g/mount/s/^/#/
	w
	q
EOM
for i in passwd master.passwd; do
	ed -s $INITFILES/etc/$i <<-EOM
		g|/root|s|/root|$MOUNTPOINT/$NAME&|
		w
		q
EOM
ed -s $INITFILES/etc/rc.conf <<-EOM
	g/^rc_configured=/s/^rc_configured=.*/rc_configured=YES/
	w
	q
EOM
cat >> $INITFILES/etc/rc.conf <<EOF
clear_tmp=NO
cron=NO
inetd=NO
savecore=NO
sendmail=NO
syslogd=NO
wscons=YES
EOF
done
touch $INITFILES/etc/fstab
(cd $INITFILES; tar -czf ../$INITFILES.tgz $RW_DIRS)
cp -r $NAME/root .
[ -d $NAME/$MOUNTPOINT ] && rmdir $NAME/$MOUNTPOINT
umount $NAME
mv $INITFILES.tgz $NAME
mv root $NAME
mkdir $NAME/pkg
vnconfig -u $VND0
vndcompress $ROOTIMAGE $ROOTZIMAGE
mv $ROOTZIMAGE $NAME
cp $KERNEL_DIRECTORY/netbsd ./bsd
makefs -s $MFDISKSIZE -t ffs $MDNAME $FAKEROOT
mdsetimage bsd $MDNAME
[ -f bsd.gz ] && rm -f bsd.gz
gzip -9 bsd
mv bsd.gz bsd
dd if=/dev/zero ibs=$BS | progress -b $BS -l ${BOOTDISKSIZE}k \
	dd of=$BOOTIMAGE count=$BOOTDISKSIZE obs=$BS
vnconfig $VND0 $BOOTIMAGE
echo "$DISKTAB" | sed "s/DISKSIZE/$BOOTDISKSIZE/g" |
	disklabel -w -f /dev/stdin /dev/r$VND0 $NAME
newfs /dev/r${VND0}a
rm -rf $BOOTDISK
mkdir $BOOTDISK
mount /dev/${VND0}a $BOOTDISK
cp /usr/mdec/boot $BOOTDISK/boot
cp bsd $BOOTDISK/bsd
installboot -v -o timeout=5 /dev/r${VND0}a /usr/mdec/bootxx_ffsv1
umount $BOOTDISK
vnconfig -u $VND0
mv bsd $NAME
echo "copy ./$NAME to device and add the following grub entry"
cat <<EOF | tee menu.lst
# GRUB menu entry
title	NetBSD
kernel	--type=netbsd /$NAME/bsd
EOF
