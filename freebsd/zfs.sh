#!/bin/sh

set -x

DISK=ada1
DISTRIBUTIONS="base.txz doc.txz games.txz lib32.txz kernel.txz src.txz"
HOSTNAME=freebsd.my.domain
SWAP_SIZE=1G

gpart create -s gpt $DISK
gpart add -s 512k -t freebsd-boot -l boot0 $DISK
gpart add -a 4k -b 1m -t freebsd-zfs -l disk0 $DISK
gpart bootcode -b /boot/pmbr -p /boot/gptzfsboot -i 1 $DISK
zpool create -m none -o cachefile=/var/tmp/zpool.cache rpool /dev/gpt/disk0
zpool export rpool
zpool import -o cachefile=/var/tmp/zpool.cache rpool
zfs set atime=off rpool
zfs create -V $SWAP_SIZE rpool/swap
zfs set org.freebsd:swap=on rpool/swap
zfs set checksum=off rpool/swap
zfs create -o mountpoint=/mnt -p rpool/ROOT/freebsd
zpool set bootfs=rpool/ROOT/freebsd rpool
zfs create -o mountpoint=/mnt/home rpool/home
zfs create -o compression=on -o setuid=off rpool/ROOT/freebsd/tmp
zfs create rpool/ROOT/freebsd/usr
zfs create rpool/ROOT/freebsd/usr/local
zfs create -o compression=on -o setuid=off rpool/ROOT/freebsd/usr/ports
zfs create -o compression=off -o exec=off -o setuid=off rpool/ROOT/freebsd/usr/ports/distfiles
zfs create -o compression=off -o exec=off -o setuid=off rpool/ROOT/freebsd/usr/ports/packages
zfs create -o compression=on -o exec=off -o setuid=off rpool/ROOT/freebsd/usr/src
zfs create -o compression=on rpool/ROOT/freebsd/usr/obj
zfs create rpool/ROOT/freebsd/var
zfs create -o compression=on -o exec=off -o setuid=off rpool/ROOT/freebsd/var/crash
zfs create -o exec=off -o setuid=off rpool/ROOT/freebsd/var/db
zfs create -o compression=on rpool/ROOT/freebsd/var/db/pkg
zfs create -o exec=off -o setuid=off rpool/ROOT/freebsd/var/empty
zfs create -o compression=on -o exec=off -o setuid=off rpool/ROOT/freebsd/var/log
zfs create -o compression=on -o exec=off -o setuid=off rpool/ROOT/freebsd/var/mail
zfs create -o exec=off -o setuid=off rpool/ROOT/freebsd/var/run
zfs create -o compression=on -o setuid=off rpool/ROOT/freebsd/var/tmp
chmod 1777 /mnt/tmp /mnt/var/tmp
(cd /mnt; ln -s home usr/home)
export DISTRIBUTIONS
bsdinstall distextract
cat > /mnt/boot/loader.conf <<EOF
zfs_load="YES"
vfs.zfs.trim_disable=0
vfs.root.mountfrom="zfs:rpool/ROOT/freebsd"
EOF
cat > /mnt/etc/rc.conf <<EOF
zfs_enable="YES"
hostname="$HOSTNAME"
EOF
touch /mnt/etc/fstab
sed -i '' "1,/^127.0.0.1/ {/^127.0.0.1/a\\
127.0.0.1		${HOSTNAME%%.*} $HOSTNAME
}" /mnt/etc/hosts
export LD_LIBRARY_PATH=/lib
chroot /mnt <<-EOF
	cd /etc/mail
	make aliases
EOF
cp /var/tmp/zpool.cache /mnt/boot/zfs/zpool.cache
zfs umount -a
zfs set mountpoint=legacy rpool/ROOT/freebsd
zfs set mountpoint=/usr rpool/ROOT/freebsd/usr
zfs set mountpoint=/var rpool/ROOT/freebsd/var
zfs set mountpoint=/tmp rpool/ROOT/freebsd/tmp
zfs set mountpoint=/home rpool/home

