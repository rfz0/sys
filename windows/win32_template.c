#include <windows.h>
#include <stdio.h>
#include <stdbool.h>

#ifndef UNICODE
#   define UNICODE
#endif /*!UNICODE*/

#ifndef MAX_CLASS_NAME_LEN
#   define MAX_CLASS_NAME_LEN 30
#endif /*!MAX_CLASS_NAME_LEN*/

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

LPTSTR  GetFormattedMessage (LPTSTR, ...);
bool    MessageLastError    (HWND  , LPCTSTR, LPCTSTR, DWORD *);
int     AskConfirmation     (HWND  , LPCTSTR, LPCTSTR);

int WINAPI
WinMain (HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine,
        INT iCmdShow)
{
    static TCHAR szAppName[MAX_CLASS_NAME_LEN];
    WNDCLASSEX   wcx;
    HWND         hwnd;
    MSG          msg;
    HANDLE       hMutex;
    DWORD        dw;
    int          i;
    UNREFERENCED_PARAMETER(hPrevInstance);

    memset(&szAppName, '\0', sizeof szAppName);
    wsprintf(szAppName, "%s", TEXT("template"), sizeof szAppName - 1);
    szAppName[sizeof szAppName] = '\0';

    hMutex = CreateMutex(NULL, TRUE, szAppName);
    if (GetLastError() == ERROR_ALREADY_EXISTS)
    {
        if (AskConfirmation(NULL, TEXT("An instance of this application is already running.")
            TEXT("Continue anyway?"), szAppName) != IDYES)
            ExitProcess(0);
    }

    ZeroMemory(&wcx, sizeof wcx);
    wcx.cbSize        = sizeof(WNDCLASSEX);
    wcx.style         = CS_HREDRAW | CS_VREDRAW;
    /*wcx.cbClsExtra    = 0;*/
    /*wcx.cbWndExtra    = 0;*/
    wcx.lpfnWndProc   = WndProc;
    wcx.hIcon         = LoadIcon(NULL, IDI_APPLICATION);
    wcx.hCursor       = LoadCursor(NULL, IDC_ARROW);
    wcx.hbrBackground = (HBRUSH) (COLOR_WINDOW + 1);
    wcx.lpszMenuName  = NULL;
    wcx.lpszClassName = szAppName;
    wcx.hIconSm       = LoadIcon(NULL, IDI_APPLICATION);

    if (!RegisterClassEx(&wcx))
    {
        MessageLastError(NULL, TEXT("RegisterClassEx"), szAppName, &dw);
        ExitProcess(dw);
    }

    hwnd = CreateWindowEx(WS_EX_CLIENTEDGE, szAppName, TEXT("Caption"),
        WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT,
        CW_USEDEFAULT, NULL, NULL, hInstance, NULL);

    if (!hwnd)
    {
        MessageLastError(NULL, TEXT("CreateWindowEx"), szAppName, &dw);
        ExitProcess(dw);
    }

    ShowWindow(hwnd, iCmdShow);
    UpdateWindow(hwnd);

    while (GetMessage(&msg, NULL, 0, 0))
    {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    UnregisterClass(szAppName, hInstance);
    
    return (int) msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    static HINSTANCE hInstance;
    static TCHAR     szAppName[MAX_CLASS_NAME_LEN];

    switch (uMsg)
    {
        case WM_CREATE:
			GetClassName(hwnd, szAppName, 
				(int) sizeof szAppName / sizeof (TCHAR));

            hInstance = ((LPCREATESTRUCT) lParam)->hInstance;
            return 0;

        case WM_CLOSE:
            if (AskConfirmation(hwnd, TEXT("Really Quit?"), szAppName) == IDYES)
                SendMessage(hwnd, WM_DESTROY, 0, 0);
            return 0;

        case WM_DESTROY:
            PostQuitMessage(0);
            return 0;
    }

    return DefWindowProc(hwnd, uMsg, wParam, lParam);
}

LPTSTR GetFormattedMessage (LPTSTR lpMsgFmt,...)
{
    LPTSTR  lpBuf;
    DWORD   dwMsgFlags = FORMAT_MESSAGE_FROM_STRING | FORMAT_MESSAGE_ALLOCATE_BUFFER;
    va_list va_args;

    va_start(va_args, lpMsgFmt);
    FormatMessage(dwMsgFlags, lpMsgFmt, 0, 0, (LPTSTR)& lpBuf, 0, &va_args);
    va_end(va_args);

    return lpBuf;
}

bool MessageLastError (HWND hwnd, LPCTSTR lpcszFuncName, LPCTSTR lpcszCaption, DWORD * dwMsgIdRet)
{
    DWORD  dwMsgId    = GetLastError ();
    LPVOID lpMsgBuf, lpDispBuf;
    DWORD  dwMsgFlags = FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS;

    *dwMsgIdRet = dwMsgId;

    lpMsgBuf = FormatMessage (dwMsgFlags, NULL, dwMsgId,
            MAKELANGID (LANG_NEUTRAL, SUBLANG_DEFAULT),
            (LPTSTR) & lpMsgBuf, 0, (va_list *) NULL);

    if (!lpMsgBuf)
        return FALSE;

    lpDispBuf = (LPVOID) GetFormattedMessage ( TEXT("%s failed with error %d: %s"),
        lpcszFuncName, dwMsgId, lpMsgBuf);

    if (lpMsgBuf)
        LocalFree(lpMsgBuf);

    if (!lpDispBuf)
        return FALSE;

    MessageBox (hwnd, (LPCTSTR) lpDispBuf, lpcszCaption, MB_ICONERROR);

    /*wprintf("%s: %s\n", (wchar_t *) lpcszCaption, (wchar_t *) lpDispBuf); */

    if (lpDispBuf)
        LocalFree (lpDispBuf);

    return TRUE;
}

int AskConfirmation(HWND hwnd, LPCTSTR lpcszQuery, LPCTSTR lpcszCaption)
{
    return MessageBox(hwnd, lpcszQuery, lpcszCaption, MB_ICONQUESTION| MB_YESNO);
}
