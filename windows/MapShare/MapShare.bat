::@echo off

set logfile=%0%.log
set xmlfile="networkdrives.xml"
set vbsfile="MapShare.vbs"
set r=0

if not [%1] == [] (
	echo usage: %0% >&2
	set r=1
) else (
	call :main
)

::pause
exit /b %r%

:main
setlocal
date /T >>%logfile%
cscript %vbsfile% %xmlfile% >>%logfile% 2>&1
endlocal
