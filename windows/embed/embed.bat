@for /f "delims=" %%a in ('powershell Get-ExecutionPolicy') do @set policy=%%a
@powershell Set-ExecutionPolicy unrestricted
@findstr /v "^@" "%~sf0" > myscript.ps1 & powershell ./myscript.ps1 & powershell Set-ExecutionPolicy %policy% & goto :eof &pause
function pause { read-host "Press any key to continue..." }
$id = [System.Security.Principal.WindowsIdentity]::GetCurrent()
$principal = new-object System.Security.Principal.WindowsPrincipal($id)
if (!$principal.IsInRole([System.Security.Principal.WindowsBuiltInRole]::Administrator)) {
	$process = new-object System.Diagnostics.ProcessStartInfo "Powershell"
	$process.Arguments = $myInvocation.MyCommand.Definition
	$process.Verb = "runas"
	[System.Diagnostics.Process]::Start($process)
	pause
	exit
}
# POWERSHELL CODE GOES HERE
pause
