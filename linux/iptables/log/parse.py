#!/usr/bin/env python

from optparse import OptionParser
import os
import re
import socket
import sys

class IPTablesLog:
    def __init__(self, opts):
        self.opts = opts
        self.d = {}

    def insert(self, d):
        if d.has_key('SRC') and d.has_key('DPT'):
            dpt = int(d['DPT'])
            if self.opts.privport and dpt > 1024:
                return
            src = d['SRC']
            if self.opts.resolve:
                if d.has_key('PROTO'):
                    try:
                        dpt = socket.getservbyport(dpt, d['PROTO'].lower())
                    except socket.error:
                        pass
                try:
                    (name, aliaslist, addrlist) = socket.gethostbyaddr(src)
                    src = name
                except socket.error:
                    pass
            k1 = src if self.opts.src else dpt
            k2 = dpt if self.opts.src else src
            if not self.d.has_key(k1):
                self.d[k1] = {}
            if not self.d[k1].has_key(k2):
                self.d[k1][k2] = 1
            else:
                self.d[k1][k2] += 1

    def open(self):
        try:
            with open(self.opts.fpath, 'r') as f:
                (i, m, n) = (0, 0, 0)
                while True:
                    l = f.readline()
                    if not l:
                        if i > 0 and self.opts.verbose:
                            sys.stderr.write('\n')
                            sys.stderr.flush()
                        break
                    yield l
                    if self.opts.verbose > 0:
                        if i <= self.opts.nlines:
                            if i == self.opts.nlines:
                                n = os.stat(self.opts.rpath).st_size
                                n /= float(m) / i
                            else:
                                m += len(l)
                        i += 1
                        if i % self.opts.verbose == 0:
                            if n > 0:
                                sys.stderr.write('\r\033[K%ld/%ld (estimated)' % (i, n))
                            else:
                                sys.stderr.write('\r\033[K%ld' % i)
                            sys.stderr.flush()
        except IOError as e:
            sys.exit(e)

    def read(self):
        g = self.open()
        try:
            while True:
                l = g.next()
                d = dict(re.findall(r'(\S+)=(\S+)', l))
                self.insert(d)
        except StopIteration:
            pass
        return self.d

def parse_opts():
    parser = OptionParser()
    parser.add_option('-f', dest='fpath', default='/var/log/iptables',
            help='path to iptables log file (e.g., /dev/stdin if PIPE)')
    parser.add_option('-F', dest='rpath', default='/var/log/iptables',
            help='path to file to sample lines from (required when using PIPE))')
    parser.add_option('-n', type='int', dest='nlines', metavar='N', default=1,
            help='sample N lines when estimating total')
    parser.add_option('-p', action='store_true', dest='privport', default=False,
            help='list results only for privileged ports (< 1024)')
    parser.add_option('-r', action='store_true', dest='resolve', default=False,
            help='resolve host names and service names')
    parser.add_option('-s', action='store_true', dest='src', default=False,
            help='list by source address rather than destination port')
    parser.add_option('-v', type='int', dest='verbose', metavar='N', default=0,
            help='print progress every N lines')

    (opts, args) = parser.parse_args()
    
    if len(args) != 0:
        parser.print_help()
        sys.exit(1)

    return opts

def main():
    opts = parse_opts()

    log = IPTablesLog(opts)
    d = log.read()

    for k1 in sorted(d.iterkeys()):
        print k1
        for k2 in d[k1].iterkeys():
            print '\t', k2, d[k1][k2]

if __name__ == '__main__':
    main()

