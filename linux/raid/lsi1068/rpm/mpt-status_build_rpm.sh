#!/bin/sh

redhat_release=`grep -Eo '[0-9.]+' /etc/redhat-release
kernel_release=`uname -r`
kernel_machine=`uname -m`
kernel_version=${kernel_release%.$kernel_machine}
kernel_package=kernel-$kernel_version.src.rpm

# create ~/rpmbuild directory tree
rpmbuild mpt-status-1.2.0.spec

wget -c ~/rpmbuild/SOURCES/http://ftp.de.debian.org/debian/pool/main/m/mpt-status/mpt-status_1.2.0.orig.tar.gz
cp mpt-status-1.2.0.spec ~/rpmbuild/SPECS
cp mpt-status_1.2.0.orig.tar.gz ~/rpmbuild/SOURCES
cp mpt-status_1.2.0.orig.tar.gz ~/rpmbuild/SOURCES
cp mpt-status-1.2.0-el6.patch ~/rpmbuild/SOURCES
wget -c http://vault.centos.org/$redhat_release/updates/Source/SPackages/$kernel_package
rpm -Uvh $kernel_package
cd ~/rpmbuild/SPECS
sudo yum -y install xmlto asciidoc elfutils-libelf-devel zlib-devel binutils-devel newt-devel python-devel perl-ExtUtils-Embed hmaccalc
rpmbuild -bp --target=$(uname -m) kernel.spec
sudo ln -s ~/rpmbuild/BUILD/kernel-$kernel_version/linux-$kernel_release /usr/src/linux
rpmbuild -ba mpt-status-1.2.0.spec
rpm -qpi ../RPMS/$kernel_machine/mpt-status-*.rpm
sudo rm /usr/src/linux
