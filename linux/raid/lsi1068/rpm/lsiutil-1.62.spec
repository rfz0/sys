Name:		lsiutil
Version:	1.62
Release:	1%{?dist}
Summary:	LSI Logic MPT Configuration Utility

Group:		Applications/System
License:	Proprietary
URL:		http://www.lsi.com
Source0:	http://www.lsi.com/downloads/Public/Obsolete/Obsolete%20Common%20Files/LSIUtil_1.62.zip
BuildRoot:	%(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

BuildRequires:	unzip

%description
LSI Logic MPT Configuration Utility, Version 1.62, January 14, 2009


%prep
%setup -c


%build


%install
rm -rf %{buildroot}
install -D -s Linux/lsiutil.%{_arch} %{buildroot}/usr/sbin/lsiutil


%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%attr(755,root,root) /usr/sbin/lsiutil
%doc changes.txt


%changelog
