Name:		mpt-status
Version:	1.2.0
Release:	1%{?dist}
Summary:	retrieve configuration and health status from LSI hardware RAID controllers

Group:		Applications/System
License:	GPL
URL:		http://www.red-bean.com/~mab/mpt-status.html
Source0:	http://ftp.de.debian.org/debian/pool/main/m/mpt-status/mpt-status_1.2.0.orig.tar.gz
Patch0:		%{name}-%{version}-el6.patch
BuildRoot:	%(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

%description
The mpt-status software is a query tool to access the running  configu-
ration  and  status of LSI SCSI HBAs.  mpt-status allows you to monitor
the health and status of your RAID setup.


%prep
%setup -q
%patch0 -p1 -b .el6


%build
make %{?_smp_mflags}


%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
make install_doc DESTDIR=%{buildroot}


%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%attr(755,root,root) /usr/sbin/mpt-status
%attr(644,root,root) /usr/share/man/man8/mpt-status.8.gz
%doc doc/*



%changelog

