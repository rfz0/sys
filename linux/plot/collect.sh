#!/bin/sh

get_cpu()
{
	# if $1 = "" then "^cpu " is matched which is the sum of all cpus
	local us1=`awk "/^cpu$1 /{print \\$2;exit}" /proc/stat`
	local ni1=`awk "/^cpu$1 /{print \\$3;exit}" /proc/stat`
	local sy1=`awk "/^cpu$1 /{print \\$4;exit}" /proc/stat`
	local id1=`awk "/^cpu$1 /{print \\$5;exit}" /proc/stat`
	sleep 1
	local us2=`awk "/^cpu$1 /{print \\$2;exit}" /proc/stat`
	local ni2=`awk "/^cpu$1 /{print \\$3;exit}" /proc/stat`
	local sy2=`awk "/^cpu$1 /{print \\$4;exit}" /proc/stat`
	local id2=`awk "/^cpu$1 /{print \\$5;exit}" /proc/stat`
	local us=`expr $us2 - $us1`
	local ni=`expr $ni2 - $ni1`
	local sy=`expr $sy2 - $sy1`
	local id=`expr $id2 - $id1`
	local used=`echo $us $ni $sy $id | awk '{printf "%.2f",(($1+$2+$3)/($1+$2+$3+$4))*100}'`
	echo "`date +%s` cpu$1 $us $ni $sy $id $used"
}

get_each_cpu()
{
	local i
	local n

	n=`grep -c '^cpu' /proc/stat`

	# don't include sum and start counting from zero
	n=`expr $n - 2`

	for i in `seq 0 $n`; do
		get_cpu $i >> cpu$i.dat &
	done
	wait
}

cpu()
{
	# get sum of all cpus
	get_cpu ""
}

mem()
{
	local total=`awk '/^MemTotal/{print $2;exit}' /proc/meminfo`
	local free=`awk '/^MemFree/{print $2;exit}' /proc/meminfo`
	local cached=`awk '/^Cached/{print $2;exit}' /proc/meminfo`
	local buffers=`awk '/^Buffers/{print $2;exit}' /proc/meminfo`
	local used=`echo $total $free $cached $buffers |
		awk '{printf "%.2f",(($1-$2-$3-$4)/$1)*100}'`
	free=`echo $total $free|awk '{printf "%.2f",($2/$1)*100}'`
	cached=`echo $total $cached|awk '{printf "%.2f",($2/$1)*100}'`
	buffers=`echo $total $buffers|awk '{printf "%.2f",($2/$1)*100}'`
	echo "`date +%s` $free $cached $buffers $used"
}

swap()
{
	local total=`awk '/^SwapTotal/{print $2;exit}' /proc/meminfo`
	local free=`awk '/^SwapFree/{print $2;exit}' /proc/meminfo`
	local used=`expr $total - $free`
	used=`echo $total $free|awk '{printf "%.2f",(($1-$2)/$1)*100}'`
	free=`echo $total $free|awk '{printf "%.2f",($2/$1)*100}'`
	echo "`date +%s` $free $used"
}

get_iface()
{
        local r1=`cat /sys/class/net/$1/statistics/rx_bytes`
        local t1=`cat /sys/class/net/$1/statistics/tx_bytes`
        sleep 1
        local r2=`cat /sys/class/net/$1/statistics/rx_bytes`
        local t2=`cat /sys/class/net/$1/statistics/tx_bytes`
        local tbps=`expr $t2 - $t1`
        local rbps=`expr $r2 - $r1`
	echo "`date +%s` $1 $rbps $tbps"
}

get_each_iface()
{
	local i

	for i in `ls /sys/class/net/`; do 
		get_iface $i >> net-$i.dat &
	done
	wait
}

get_disk()
{
	local r1=`awk "/ $1 /{print \\$6;exit}" /proc/diskstats`
	local w1=`awk "/ $1 /{print \\$10;exit}" /proc/diskstats`
	sleep 1
	local r2=`awk "/ $1 /{print \\$6;exit}" /proc/diskstats`
	local w2=`awk "/ $1 /{print \\$10;exit}" /proc/diskstats`
	local rbps=`expr \( $r2 - $r1 \) \* 512`
	local wbps=`expr \( $w2 - $w1 \) \* 512`
	echo "`date +%s` $1 $rbps $wbps"
}

get_each_disk()
{
	local i

	for i in `awk '{if ($1=='8') print $3}' /proc/diskstats`; do
		get_disk $i >> disk-$i.dat &
	done
	wait
}

load()
{
	echo "`date +%s` `awk '{print $1,$2,$3}' /proc/loadavg`"	
}

cpu >> cpu.dat &
get_each_cpu &
load >> load.dat &
mem >> mem.dat &
swap >> swap.dat &
get_each_disk &
get_each_iface &
wait
