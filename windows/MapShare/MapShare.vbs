Option Explicit

Class NetworkDrive
	Private m_bUpdateProfile
	Private m_strDescription
	Private m_strDriveName
	Private m_strPassword
	Private m_strRemotePath
	Private m_strUserName

	Public Default Function Init(strDriveName, strRemotePath, bUpdateProfile, _
			strUserName, strPassword, strDescription, bMapReverse)
		If IsNull(strDriveName) Or IsEmpty(strDriveName) Or LCase(strDriveName) = "free" Then
			m_strDriveName = NextFreeDrive(bMapReverse)
			if IsNull(m_strDriveName) Then
				WScript.Echo "No free drive found."
			End If
		Else
			SetDriveName(strDriveName)
		End If
		m_bUpdateProfile = bUpdateProfile
		m_strDescription = strDescription
		m_strPassword = strPassword
		m_strRemotePath = strRemotePath
		m_strUserName = strUserName
		Set Init = Me
	End Function

	Public Property Get strDriveName()
		strDriveName = m_strDriveName
	End Property

	Public Property Let strDriveName(a_strDriveName)
		SetDriveName(a_strDriveName)
	End Property

	Private Sub SetDriveName(a_strDriveName)
		If ValidateDriveName(a_strDriveName) Then
			If Not DriveExists(a_strDriveName) Then
				m_strDriveName = a_strDriveName
			Else
				WScript.Echo "Drive already exists."
			End If
		Else
			WScript.Echo "Drive name is not valid."
		End If
	End Sub

	Public Function ValidateDriveName(strDriveName)
		Dim bValid : bValid = False

		If Not IsNull(strDriveName) And Not IsEmpty(strDriveName) Then
			Dim objRegExp : Set objRegExp = CreateObject("VBScript.RegExp")
			objRegExp.Global = True : objRegExp.Pattern = "[a-zA-Z]:"
			Dim colMatches : Set colMatches = objRegExp.Execute(strDriveName)
			Set objRegExp = Nothing
			If colMatches.Count = 1 Then
				bValid = True
			End If
			Set colMatches = Nothing
		End If

		ValidateDriveName = bValid
	End Function

	Public Function DiskExists(strDiskName)
		Const strComputer = "."
		Dim objWMIService : Set objWMIService = getObject("winmgmts:\\" & strComputer & "\root\cimv2")
		Dim colDisks : Set colDisks = objWMIService.ExecQuery("Select * from Win32_LogicalDisk")

		Dim bExists : bExists = False
		Dim objDisk
		For Each objDisk in colDisks
			If StrComp(objDisk.DeviceID, strDiskName,  vbTextCompare) = 0 Then
				bExists = True
				Exit For
			End If
		Next

		Set objDisks = Nothing
		Set objWMIService = Nothing

		Set DiskExists = bExists
	End Function

	Public Function DriveExists(strDriveName)
		Dim objWSHFileSystem : Set objWSHFileSystem = CreateObject("Scripting.FileSystemObject")
		DriveExists = objWSHFileSystem.DriveExists(strDriveName)
		Set objWSHFileSystem = Nothing
	End Function

	Public Function NextFreeDrive(bMapReverse)
		Dim objWSHFileSystem : Set objWSHFileSystem = CreateObject("Scripting.FileSystemObject")
		Dim strAlphabet : strAlphabet = "a b c d e f g h i j k l m n o p q r s t u v w x y z"
		Dim objDrives : Set objDrives = objWSHFileSystem.Drives
		Dim intIncrement : intIncrement = 1

		If bMapReverse = True Then
			strAlphabet = StrReverse(strAlphabet)
			intIncrement = -1
		End If

		Dim arrAlphabet : arrAlphabet = Split(strAlphabet)

		Dim intStartLetter : intStartLetter = Asc(arrAlphabet(0))
		Dim intEndLetter : intEndLetter = intIncrement + Asc(arrAlphabet(UBound(arrAlphabet)))
		While objWSHFileSystem.DriveExists(Chr(intStartLetter)+":") And intStartLetter <> intEndLetter
			intStartLetter = intStartLetter + intIncrement
		Wend

		If Not intStartLetter = intEndLetter Then
			NextFreeDrive = UCase(Chr(intStartLetter))+":"
		End If

		Set objWSHFileSystem = Nothing
	End Function

	Private Sub MapNetworkDrive(strDriveName, strRemotePath, bUpdateProfile, strUserName, strPassword)
		On Error Resume Next

		Dim objWSHNetwork

		Set objWSHNetwork = WScript.CreateObject("WScript.Network")
		objWSHNetwork.MapNetworkDrive strDriveName, strRemotePath, bUpdateProfile, _
			strUserName, strPassword
		If Err.Number <> 0 Then
			WScript.Echo "Failed to map network drive." & vbCrLf & _
			vbCrLf & _
			"Drive Letter: " & vbTab & strDriveName & vbCrLf & _
			"Remote Path: " & vbTab & strRemotePath & vbCrLf & _
			vbCrLf & _
			"Description: " & vbTab & Err.Description
			Err.Clear
		End If

		Set objWSHNetwork = Nothing
	End Sub

	Public Function Map()
		MapNetworkDrive m_strDriveName, m_strRemotePath, m_bUpdateProfile, _
			m_strUserName, m_strPassword
	End Function
End Class


Class FileSystem
	Public Sub CreateShortcut(strSourcePath, strDestinationPath, strDescription)
		On Error Resume Next

		Dim objWSHShell : Set objWSHShell = Wscript.CreateObject("Wscript.Shell")
		Dim objShortcut : Set objShortcut = objWSHShell.CreateShortcut(strDestinationPath & ".lnk")

		objShortcut.TargetPath = strSourcePath
		objShortcut.Description = strDescription
		objShortcut.Save()
		If Err.Number <> 0 Then
			WScript.Echo "Failed to save shortcut." & vbCrLf & _
			vbCrLf & _
			"Source Path: " & vbTab & strSourcePath & vbCrLf & _
			"Destination Path: " & vbTab & strDestinationPath & vbCrLf & _
			vbCrLf & _
			"Description: " & vbTab & Err.Description
			Err.Clear
		End If

		Set objShortcut = Nothing
		Set objWSHShell = Nothing
	End Sub

	Public Sub CreateDesktopShortcut(strSourcePath, strDestinationPath, strDescription)
		Dim objWSHShell : Set objWSHShell = Wscript.CreateObject("Wscript.Shell")
		Dim objDesktop : objDesktop = objWSHShell.SpecialFolders("Desktop")
		
		CreateShortcut strSourcePath, objDesktop & "\" & strDestinationPath, strDescription

		Set objDesktop = Nothing
		Set objWSHShell = Nothing
	End Sub
End Class

class XMLDocument
	Private m_objXMLDocument

	Public Default Function Init(strXMLDocument)
		If Not IsNull(strXMLDocument) Then
			Set m_objXMLDocument = LoadDocument(strXMLDocument)
		End If
		Set Init = Me
	End Function

	Public Property Get objXMLDocument()
		Set objXMLDocument = m_objXMLDocument
	End Property

	Public Property Set objXMLDocument(a_objXMLDocument)
		Set m_objXMLDocument = a_objXMLDocument
	End Property

	Public Function LoadDocument(strXMLDocument)
		Dim objDOMDocument : Set objDOMDocument = CreateObject("MSXML2.DOMDocument.3.0")

		objDOMDocument.validateOnParse = False
		If Not objDOMDocument.Load(strXMLDocument) Then
			Dim objParseError : Set objParseError = objDOMDocument.parseError

			WScript.Echo "Failed to load XML document." & vbCrLf & _
			vbCrLf & _
			"Document: " & vbTab & strXMLDocument & vbCrLf & _
			vbCrLf & _
			"Description: " & objParseError.reason
			objParseError = Nothing
		End If

		Set LoadDocument = objDOMDocument
	End Function
End Class

Class NetworkDriveMapper
	Private m_bMapReverse
	Private m_strTagName
	Private m_objXMLDocument

	Public Default Function Init(strXMLDocument)
		Set m_objXMLDocument = (New XMLDocument)(strXMLDocument)
		m_bMapReverse = True
		m_strTagName = "networkdrive"
		Set Init = Me
	End Function

	Public Property Get bMapReverse()
		bMapReverse = m_bMapReverse
	End Property

	Public Property Let bMapReverse(a_bMapReverse)
		m_bMapReverse = a_bMapReverse
	End Property

	Public Sub Map()
		If Not m_objXMLDocument.objXMLDocument Is Nothing Then
			Dim objNodeList : Set objNodeList = m_objXMLDocument.objXMLDocument.getElementsByTagName(m_strTagName)
			Dim objNode
			For Each objNode in objNodeList
				If objNode.hasChildNodes Then
					Dim objChildNodeList : Set objChildNodeList = objNode.childNodes
					Dim i
					For i=0 To objChildNodeList.length-1 Step 1
						Dim strText : strText = Trim(objChildNodeList.Item(i).text)
						Dim bUpdateProfile, strDescription, strDriveName, strPassword, strRemotePath, _
							 strShareName, strUserName
						Select Case objChildNodeList.Item(i).nodeName
							Case "description" : strDescription = strText
							Case "drivename" : strDriveName = strText
							Case "password" : strPassword = strText
							Case "remotepath" : strRemotePath = strText
							Case "sharename" : strShareName = strText
							Case "updateprofile" : bUpdateProfile = strText
							Case "username" : strUserName = strText
						End Select
					Next
					Set objChildNodeList = Nothing
					Dim objNetworkDrive : Set objNetworkDrive = (New NetworkDrive) _
						(strDriveName, "\\" & strRemotePath & "\" & strShareName, bUpdateProfile, _
							strUserName, strPassword, strShareName, bMapReverse)
					objNetworkDrive.Map()
					strDriveName = objNetworkDrive.strDriveName
					If Not IsNull(strDriveName) And Not IsEmpty(strDriveName) And strDriveName <> "empty" Then
						Dim objFileSystem : Set objFileSystem = New FileSystem
						objFileSystem.CreateDesktopShortcut strDriveName, strShareName, strShareName
						Set objFileSystem = Nothing
					End If
					Set objNetworkDrive = Nothing
				End If
			Next
			Set objNode = Nothing
			Set objNodeList = Nothing
		End If
	End Sub
End Class

If WScript.Arguments.Count <> 1 Then
	WScript.Echo "usage: " & WScript.ScriptFullName & " " & "[xml file]"
	WScript.Quit(1)
End If

Dim strXMLDocumentFileName : strXMLDocumentFileName = WScript.Arguments.Item(0)
Dim objNetworkDriveMapper : Set objNetworkDriveMapper = (New NetworkDriveMapper)(strXMLDocumentFileName)
objNetworkDriveMapper.bMapReverse = True
objNetworkDriveMapper.Map()
Set objNetworkDriveMapper = Nothing
WScript.Quit(0)
