#!/bin/sh
#
# Encrypt /home partition and mount /tmp on MFS.
#

[ "$DEBUG" ] && set -x
wipe=YES
mfs_tmp=YES
mfs_tmp_size=262144

duid_to_diskname()
{
	sysctl -n hw.disknames |
	awk -v d=$1 -F, '{
		for (i=1; i<=NF; i++) {
			split($i, a, ":")
			if (a[2] == d) {
				print a[1]
				exit
			}
		}
	}'
}

diskname_to_duid()
{
	disklabel $1 | grep ^duid: | cut -d' ' -f2
}


mount | grep -qs home && umount /home || exit 1

fs_spec=`awk '{ if ($2 == "/home") { print $1; exit } }' /etc/fstab`
[ "$fs_spec" ] || exit 1
fs_spec=${fs_spec##*/}
part=${fs_spec##*.}
if [ "$part" ]; then
	#  fstab uses DUID
	duid=${fs_spec%.*}
	disk=`duid_to_diskname $duid`
else
	# fstab uses device name
	disk=${fs_spec%[0-9]*}
	part=${fs_spec##*[0-9]}
	duid=`diskname_to_duid $disk`
fi

# edit label
printf "m $part\n\n\nRAID\nw\nq\n" | disklabel -E $disk

# nuke partition
[ X"$wipe" = X"YES" ] && dd if=/dev/random of=/dev/$disk$part bs=1m

# configure softraid device
until bioctl -c C -l /dev/$disk$part softraid0; do sleep 1; done
cdisk=`dmesg | grep "SR CRYPTO" | cut -d' ' -f1 | tail -1`

# from bioctl(8):
# After creating a newly encrypted disk, the first megabyte of it should
# be zeroed, so tools like fdisk(8) or disklabel(8) don't get confused by
# the random data that appears on the new disk.
dd if=/dev/zero of=/dev/r${cdisk}c bs=1m count=1

# initialize softraid device
fdisk -iy $cdisk

# create a single partition for /home
printf "a\n\n\n\n\nq\ny\n" | disklabel -E $cdisk

# create fs
newfs /dev/r${cdisk}a
#fsck -f -y /dev/r${cdisk}a

# edit fstab
cduid=`diskname_to_duid $cdisk`
ed -s /etc/fstab <<-EOF
	/home/d
	a
	$cduid.a /home ffs noauto,rw,noatime,nodev,nosuid 0 0
	.
	w
	q
EOF

mount /home

# automate at startup and shutdown
cat >> /etc/rc.local << EOF
until bioctl -c C -l /dev/$disk$part softraid0; do done
fsck -p -y /dev/r${cdisk}a
mount /home
EOF

cat >> /etc/rc.shutdown << EOF
umount /home
bioctl -d $cdisk
EOF

# setup tmp on mfs
if [ X"$mfs_tmp" = X"YES" ]; then
	umount /tmp
	ed -s /etc/fstab <<-EOF
		/tmp/d
		a
		swap /tmp mfs rw,-s=$mfs_tmp_size,noatime,async,nodev,nosuid,noexec 0 0
		.
		w
		q
	EOF
	chmod 1777 /tmp
	mount /tmp
fi
